'use strict';

const API = require('./API');
const cronstrue = require('cronstrue');

class Process extends API {
  constructor(api) {
    super(api);

    this.name = null;
    this.description = '';
    this.schedule = null;
    this.priority = 1000;
    this.allowed = [];
    //this.allowed = ['web', 'websocket', 'grpc', 'task'];
  }

  async init() {
    await this.api.log(`[${this.priority}] Process '${this.name}' initializing`, 'debug');
    if (this.schedule !== null) {
      let scheduleText = cronstrue.toString(this.schedule, { locale: this.api.config.app.locale });
      this.api.log(`Process '${this.name}' scheduled to run ${scheduleText}`);
    }
  }

  async pre() {
  }

  async exec(data) {
  }

  async post() {
  }

  async shutdown() {
    await this.api.log(`[${this.priority}] Process '${this.name}' stopping`, 'debug');
  }

  isAllowed(connectionType) {
    return this.allowed.indexOf(connectionType) > -1 || this.allowed.indexOf('all') > -1;
  }
}

module.exports = Process;
