'use strict';

const URL = require('url');
const Agenda = require('agenda');
const { MongoClient } = require('mongodb');
const Component = require('./Component');
const cluster = require('cluster');

class TaskManager extends Component {
  constructor(api) {
    super(api);

    if (this.api.config.scheduler.enabled !== true) {
      throw new Error(`config.scheduler.enabled = ${this.api.config.scheduler.enabled}, must be true`);
    }
  }

  async initialize() {
    await super.initialize();

    await super.start();

    // eslint-disable-next-line node/prefer-global/url
    const dbUrl = new URL.URL(this.api.config.scheduler.connectionUrl);
    this._client = new MongoClient(dbUrl.toString(), {
      useNewUrlParser: true
    });

    // Use connect method to connect to the Server
    await this._client.connect();
    this.api.log(`Connected to ${dbUrl.toString()}`);

    const db = this._client.db(dbUrl.pathname.replace('/', ''));

    let name = 'taskManager';
    /*let name = 'master';
    if (cluster.isWorker === true && cluster.worker) {
      name = `worker #${cluster.worker.id}`;
    }*/

    this._agenda = new Agenda({ name }).mongo(db, 'jobs');

    this._agenda.define('hello world', () => {
      this.api.log('Hello, World!', 'info');
    });

    // Wait until Agenda is ready
    await new Promise(resolve => this._agenda.once('ready', resolve));

    // Start tracking jobs to be performed
    this._agenda.start();

    // TODO: Test jobs; remove this loop
    let jobs = new Array(0);
    let i = 0;
    jobs.fill('a', 0, jobs.length);
    // eslint-disable-next-line no-unused-vars
    for (const job of jobs) {
      await this.schedule(new Date(Date.now() + 1000 + i++), 'hello world');
    }
  }

  async start() {
    this.api.log('Task manager ready');
  }

  async define(name, fn) {
    this._agenda.define(name, fn);
  }

  async schedule(time, name) {
    this.api.log(`Scheduling '${name}' to occur at ${time}`, 'debug');
    this._agenda.every(time, name);
  }

  async stop() {
    await super.stop();
    await this._client.close();
  }
}

module.exports = TaskManager;
