'use strict';

const Component = require('./Component');

class Server extends Component {
  async start() {}

  async stop() {}
}

module.exports = Server;
