'use strict';

const API = require('./API');

class Component extends API {
  async initialize() {
  }

  async start() {
  }

  async stop() {
  }
}

module.exports = Component;
