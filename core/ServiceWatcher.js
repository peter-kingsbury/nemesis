'use strict';

const Component = require('./Component');

class ServiceWatcher extends Component {
  constructor(api, serviceName, healthcheckFunction, success, interval, timeout, failureCount) {
    super(api);

    if (timeout < interval) {
      throw new Error(`interval ${interval} must be greater than timeout ${timeout}`);
    }

    this.serviceName = serviceName;
    this.healthcheckFunction = healthcheckFunction;
    this.successCriteria = success;
    this.checkInterval = interval;
    this.checkTimeout = timeout;
    this.failureCount = failureCount;

    this.failures = 0;
    this.timer = null;
  }

  async start() {
    await super.start();
    if (this.timer !== null) {
      throw new Error('timer already started');
    }

    this.timer = setInterval(async () => {
      await this.watch();
    }, this.checkInterval);
  }

  async watch() {
    const check = new Promise((resolve, reject) => {
      return this.healthcheckFunction().then(result => {
        if (result !== this.successCriteria) {
          return reject();
        }
        resolve();
      });
    });

    const timeout = new Promise((resolve, reject) => {
      setTimeout(reject, this.checkTimeout, 'race');
    });

    try {
      await Promise.race([check, timeout]);

      if (this.failures > 0) {
        this.api.log(`Service health-check for '${this.serviceName}' recovered after ${this.failures} failure${this.failures === 1 ? '' : 's'}`, 'info');
      }

      this.failures = 0;
    } catch (e) {
      this.failures++;

      this.api.log(`Service health-check for '${this.serviceName}' failed (count=${this.failures})`, 'warn');

      if (this.failures >= this.failureCount) {
        this.api.log(`Service health-check for "${this.serviceName}" failed ${this.failures} times; shutting down...`, 'error');
        // eslint-disable-next-line no-process-exit
        return process.exit(1);
      }
    }
  }
}

module.exports = ServiceWatcher;
