'use strict';

class API {
  constructor(api) {
    if (typeof api !== 'object') {
      throw new Error('invalid api object provided');
    }
    this.api = api;
  }
}

module.exports = API;
