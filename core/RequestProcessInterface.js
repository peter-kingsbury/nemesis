'use strict';

const Component = require('./Component');

class RequestProcessInterface extends Component {
  constructor(api) {
    super(api);

    this.interfaces = {
      web: {},
      websocket: {},
      grpc: {}
    };
  }

  async configure() {
    await this.configureWeb();
    await this.configureTasks();
  }

  async configureWeb() {
    // Bind web routes to processes
    const routes = this.api.config.servers.web.routes;
    const routesList = Object.keys(routes);
    const webServer = this.api.servers.web;
    if (!webServer) {
      return;
    }

    for (const routeKey of routesList) {
      // eslint-disable-next-line security/detect-object-injection
      const route = routes[routeKey];
      // console.log(route);

      for (const [method, endpoint] of Object.entries(route)) {
        if (!this.api.processes.hasOwnProperty(endpoint)) {
          this.api.log(`Route '${method.toUpperCase()} ${routeKey}' maps to Process '${endpoint}' but no such Process exists`, 'error');
        }

        // eslint-disable-next-line security/detect-object-injection
        const process = this.api.processes[endpoint];
        if (!process.isAllowed('web')) {
          this.api.log(`Route '${method.toUpperCase()} ${routeKey}' maps to Process '${endpoint}' but 'all' or 'web' are not in allowed list of connection types`, 'error');
          continue;
        }

        this.api.log(`'${method.toUpperCase()} ${routeKey}' maps to Process '${endpoint}'`);
        webServer.addRoute(method, routeKey, (req, res) => {
          let start = Date.now();
          let data = this.transformWebRequest(req);
          process.exec(data).then(result => {
            // TODO: Transform responses properly
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify(result));
            let end = Date.now();
            this.api.log(`${method} ${routeKey} ${JSON.stringify(this.scrub(data.params))} (${end - start} ms)`);
          }).catch(e => {
            this.api.log(e, 'error');
            // TODO: Write errors properly
            res.status(500).end();
          });
        });
      }
    }
  }

  transformWebRequest(req) {
    return {
      params: req.params,
      raw: req
    };
  }

  scrub(params) {
    // TODO: Actually scrub params
    return params;
  }

  async configureTasks() {
    const processes = this.api.processes;
    const taskManager = this.api.taskManager;
    const processKeys = Object.keys(this.api.processes);
    for (const key of processKeys) {
      const process = processes[key];
      if (!process.isAllowed('task')) {
        continue;
      }
      await taskManager.define(process.name, data => {
        let start = Date.now();
        this.api.log(`Running ${process.name} ...`, 'debug');
        process.exec(data).then(() => {
          // TODO: Decide what will be done with result of exec() -- chaining? reschedule?
          let end = Date.now();
          this.api.log(`${process.name} ${JSON.stringify(this.scrub(data.params))} (${end - start} ms)`);
        }).catch(e => {
          // TODO: Decide what will be done with error from exec() -- deschedule on failure?
          this.api.log(e, 'error');
        });
      });

      await taskManager.schedule(process.schedule, process.name);
    }
  }
}

module.exports = RequestProcessInterface;
