'use strict';

const util = require('util');
const path = require('path');
const fs = require('fs');
const cluster = require('cluster');
// eslint-disable-next-line security/detect-non-literal-fs-filename
const fsReadDirAsync = util.promisify(fs.readdir);
// TODO: Remove request-promise and example
// eslint-disable-next-line node/no-unpublished-require
const rp = require('request-promise');
const Config = require('./config/Config.js');
const Logger = require('./components/Logger.js');
const Web = require('./servers/Web.js');
const WebSocket = require('./servers/WebSocket.js');
const GRPC = require('./servers/GRPC.js');
const ServiceWatcher = require('./core/ServiceWatcher.js');
const TaskManager = require('./core/TaskManager');
const RequestProcessInterface = require('./core/RequestProcessInterface');

const UNCAUGHT_EXCEPTION = 1;

// eslint-disable-next-line no-unused-vars
function debugObj(obj) {
  // eslint-disable-next-line
  console.log(util.inspect(obj, {showHidden: false, depth: null}));
}

class Nucleus {
  constructor() {
    this.Process = require('./core/Process');
    this.Initializer = require('./core/Initializer');
  }

  async initialize() {
    this.servers = {
      web: null,
      websocket: null,
      grpc: null
    };

    this.taskManager = null;

    this.dependentServices = {};

    this.processes = {};

    // Load config
    this.config = await (new Config()).load();

    // Load the logger
    this.logger = new Logger(this);
    await this.logger.initialize(this.config.logger);

    if (this.config.app.cluster.enabled === true && cluster.isMaster) {
      await this.initializeMaster();

      cluster.on('online', worker => {
        this.log(`Worker ${worker.process.pid} is online`, 'info');
      });

      cluster.on('exit', (worker, code, signal) => {
        this.log(`Worker ${worker.process.pid} died with code ${code} and signal ${signal}`, 'warn');
        this.log('Starting a new worker', 'info');
        cluster.fork();
      });
    } else {
      await this.initializeWorker();
    }
  }

  async initializeMaster() {
    // TODO: Use this.config.app.cluster.workers
    const cpuCount = require('os').cpus().length;
    for (let i = 0; i < cpuCount; i++) {
      this.log(`Starting worker #${i}`);
      cluster.fork();
    }
  }

  async initializeWorker() {
    // TODO: Unify naming convention of taskManager, scheduler, etc
    await this.subscribeToSignalEvents();

    await this.loadProcesses();

    await this.initializeServers();

    await this.initializeTaskManager();

    await this.initializeRPI();

    // TODO: Remove this
    await this.setupTestMonitoring();
  }

  async subscribeToSignalEvents() {
    this.config.app.shutdownSignals.forEach(sig => {
      process.on(sig, () => {
        this.stop(sig).then(() => {
          // eslint-disable-next-line no-process-exit
          return process.exit(0);
        });
      });
    });

    this.log(`Subscribed to shutdown events ${this.config.app.shutdownSignals}`);

    process.on('exit', code => {
      this.log(`Exiting with code ${code}`);
    });

    // TODO: Finish unhandledRejection
    process.on('unhandledRejection', this.stop.bind(this, UNCAUGHT_EXCEPTION));

    // TODO: Improve once issue https://github.com/nodejs/node/issues/24321 is resolved
    process.on('multipleResolves', (type, promise, reason) => {
      if (reason === 'race') {
        // Ignore race conditions due to usage of Promise.race
        return;
      }
      this.log(`Event 'multipleResolves' raised, type=${type} promise=${JSON.stringify(promise)} reason=${reason}`, 'warn');
    });

    process.on('uncaughtException', this.stop.bind(this, UNCAUGHT_EXCEPTION));
  }

  async setupTestMonitoring() {
    let url = 'https://postman-echo.com/get';

    const checkFunc = async () => {
      try {
        await rp(url);
        return true;
      } catch (e) {
        this.log(e.message, 'error');
        return false;
      }
    };

    await this.registerDependentService('postman', checkFunc, true, 1000, 5000, 5);
  }

  async initializeServers() {
    if (this.config.servers.web.enabled === true) {
      this.log('Initializing web server');
      this.servers.web = new Web(this);
    }

    if (this.config.servers.websocket.enabled === true) {
      if (this.servers.web && this.servers.web.server) {
        this.log('Initializing websocket server');
        this.servers.websocket = new WebSocket(this, this.servers.web.server);
      } else {
        this.log('Websocket server is enabled but no web server is running', 'warn');
      }
    }

    if (this.config.servers.grpc.enabled === true) {
      this.log('Initializing grpc server');
      this.servers.grpc = new GRPC(this);
    }
  }

  async stopServers() {
    if (this.servers.websocket) {
      await this.servers.websocket.stop();
    }

    if (this.servers.web) {
      await this.servers.web.stop();
    }

    if (this.servers.grpc) {
      await this.servers.grpc.stop();
    }
  }

  async registerDependentService(serviceName, healthcheckFunction, success, interval, timeout, failureCount) {
    if (this.dependentServices.hasOwnProperty(serviceName)) {
      throw new Error(`unable to register dependent service with name ${serviceName}; service name already exists`);
    }

    // eslint-disable-next-line security/detect-object-injection
    this.dependentServices[serviceName] = new ServiceWatcher(this, serviceName, healthcheckFunction, success, interval, timeout, failureCount);

    this.log(`Registering dependent service '${serviceName}'`, 'debug');

    // eslint-disable-next-line security/detect-object-injection
    await this.dependentServices[serviceName].start();

    this.log(`Watcher for service '${serviceName}' started`, 'debug');
  }

  async initializeTaskManager() {
    if (this.config.scheduler.enabled === true) {
      this.log('Initializing task manager');
      this.taskManager = new TaskManager(this);
      await this.taskManager.initialize();
    }
  }

  async loadProcesses() {
    let processDir = path.join(process.cwd(), 'processes');

    this.log(`Loading process files from ${processDir} ...`, 'debug');

    let files = await fsReadDirAsync(processDir);
    for (const file of files) {
      const processPath = path.join(processDir, file);
      // eslint-disable-next-line security/detect-non-literal-require
      const Process = require(processPath);
      let process = new Process(this);

      if (process.name === null) {
        throw new Error(`Class at '${processPath}' does not have a valid name`);
      }

      if (this.processes.hasOwnProperty(process.name)) {
        throw new Error(`Class at '${processPath}' duplicates name ${process.name}`);
      }

      let parentName = Object.getPrototypeOf(process.constructor).name;
      if (parentName !== 'Process') {
        throw new TypeError(`Class at '${processPath}' does not inherit from Process`);
      }

      this.log(`Loaded process ${file}`, 'debug');
      this.processes[process.name] = process;
    }

    // TODO: Check for duplicate-priority Processes

    const processesByPriority = Object.values(this.processes).sort((p1, p2) => {
      return p1.priority - p2.priority;
    });

    for (const process of processesByPriority) {
      await process.init();
    }
  }

  async initializeRPI() {
    this.rpi = new RequestProcessInterface(this);
    await this.rpi.configure();
  }

  async start() {
    if (this.config.servers.web.enabled === true && this.servers.web) {
      await this.servers.web.start();
    }

    if (this.config.servers.websocket.enabled === true && this.servers.websocket) {
      await this.servers.websocket.start();
    }

    if (this.config.servers.grpc.enabled === true && this.servers.grpc) {
      await this.servers.grpc.start();
    }

    if (this.config.scheduler.enabled === true && this.taskManager) {
      await this.taskManager.start();
    }
  }

  async stop(signal) {
    await this.log(`Received signal ${signal}; stopping`);

    const processesByPriority = Object.values(this.processes).sort((p1, p2) => {
      return p2.priority - p1.priority;
    });

    for (const process of processesByPriority) {
      await process.shutdown();
    }

    await this.stopServers();

    await this.log('Goodbye!');
  }

  async log(message, level) {
    await this.logger.log(message, level);
  }
}

// TODO: Singleton
module.exports = new Nucleus();
