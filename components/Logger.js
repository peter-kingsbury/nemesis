'use strict';

const Component = require('../core/Component');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
const cluster = require('cluster');

// TODO: Introduce object inspection via log function
// TODO: Consult attribute blacklist (gotten from config) when outputting objects with certain attributes (password, etc)

class Logger extends Component {
  async initialize(cfg) {
    if (!cfg.enabled) {
      return;
    }

    this._base = createLogger({
      exitOnError: false,
      level: cfg.level,
      format: combine(
        format.colorize(),
        // format.json(),
        label({ label: cfg.label }),
        timestamp(),
        printf(info => {
          if (cluster.isMaster) {
            return `${info.timestamp} [${info.label}] [master] ${info.level}: ${info.message}`;
          } else {
            return `${info.timestamp} [${info.label}] [worker #${cluster.worker.id}] ${info.level}: ${info.message}`;
          }
        })
      )
    });

    for (const key of Object.keys(cfg.transports)) {
      switch (key) {
      case 'console':
        this._base.add(new transports.Console());
        break;

      // TODO: Add https://github.com/winstonjs/winston-daily-rotate-file
      // TODO: Set dirname='./logs', maxSize='10m', maxFiles='10'
      case 'file':
        for (const f of cfg.transports.file) {
          this._base.add(new transports.File(f));
        }
        break;
      }
    }
  }

  async log(message, level = 'info') {
    await this._base.log({
      level,
      message
    });
  }
}

module.exports = Logger;
