'use strict';

(async () => {
  const { Nucleus } = require('..');
  const n = new Nucleus();
  await n.initialize();

  n.log('hello, world!');

  await n.start();
  await n.stop();
})();
