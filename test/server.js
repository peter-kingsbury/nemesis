'use strict';

const { describe, it } = require('mocha');
const { Nucleus } = require('../index');

describe('Server Tests', () => {
  it('load the base class', async () => {
    let n = new Nucleus();

    await n.initialize();
    await n.start();
  });
});
