'use strict';

const Process = require('../core/Process');

class TestProcess extends Process {
  constructor(api) {
    super(api);

    this.name ='test process';
    this.allowed = ['web'];
    this.priority = 1100;
  }

  async exec(data) {
    return {};
  }
}

module.exports = TestProcess;
