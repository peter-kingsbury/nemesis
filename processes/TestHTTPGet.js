'use strict';

const Process = require('../core/Process');

class TestHTTPGet extends Process {
  constructor(api) {
    super(api);

    this.name ='test http get process';
    this.allowed = ['web'];
    this.spec = {
      method: 'get',
      route: '/abc'
    };
  }

  async exec(data) {
    return {};
  }
}

module.exports = TestHTTPGet;
