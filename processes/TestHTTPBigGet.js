/* eslint-disable quotes */
'use strict';

const Process = require('../core/Process');

class TestHTTPGet extends Process {
  constructor(api) {
    super(api);

    this.name ='test http big get process';
    this.allowed = ['web'];
  }

  async exec(data) {
    return [
      {
        "_id": "5c1ce50447855cde1fc9883e",
        "index": 0,
        "guid": "1ccb1337-1eec-4a90-bcdf-40a7261a7729",
        "isActive": false,
        "balance": "$2,732.16",
        "picture": "http://placehold.it/32x32",
        "age": 36,
        "eyeColor": "brown",
        "name": {
          "first": "Wanda",
          "last": "Kelly"
        },
        "company": "HOPELI",
        "email": "wanda.kelly@hopeli.io",
        "phone": "+1 (857) 547-2063",
        "address": "830 Langham Street, Sena, Ohio, 8546",
        "about": "Ipsum et duis incididunt ea. Dolore aute sit Lorem adipisicing officia. Ut tempor pariatur esse aliquip consequat deserunt nulla magna enim.",
        "registered": "Saturday, September 19, 2015 6:08 PM",
        "latitude": "-44.598028",
        "longitude": "-100.754536",
        "tags": [
          "laboris",
          "amet",
          "ad",
          "tempor",
          "elit"
        ],
        "range": [
          0,
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9
        ],
        "friends": [
          {
            "id": 0,
            "name": "Rodriguez Cain"
          },
          {
            "id": 1,
            "name": "Myrtle Mcguire"
          },
          {
            "id": 2,
            "name": "Zimmerman Luna"
          }
        ],
        "greeting": "Hello, Wanda! You have 7 unread messages.",
        "favoriteFruit": "banana"
      },
      {
        "_id": "5c1ce504c53901460f127b75",
        "index": 1,
        "guid": "fa51cc25-dcda-44ef-8652-051cf5de5ddd",
        "isActive": true,
        "balance": "$1,844.55",
        "picture": "http://placehold.it/32x32",
        "age": 27,
        "eyeColor": "brown",
        "name": {
          "first": "Meredith",
          "last": "Palmer"
        },
        "company": "MEDIOT",
        "email": "meredith.palmer@mediot.net",
        "phone": "+1 (831) 509-3148",
        "address": "799 Revere Place, Sardis, Virginia, 3043",
        "about": "Qui aliqua aute reprehenderit officia nulla Lorem adipisicing do ea anim nostrud aliqua enim esse. Enim aliqua laborum in ipsum laboris aliqua. Aliquip sunt pariatur aliqua culpa reprehenderit officia dolore. Qui dolor eu ad in. Id dolor eu officia veniam enim ea veniam velit adipisicing minim non eiusmod nisi. Occaecat ad occaecat mollit consectetur proident. Sit deserunt dolor eiusmod aliqua.",
        "registered": "Saturday, October 3, 2015 10:29 AM",
        "latitude": "66.035478",
        "longitude": "147.063004",
        "tags": [
          "aliquip",
          "ea",
          "qui",
          "ipsum",
          "ipsum"
        ],
        "range": [
          0,
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9
        ],
        "friends": [
          {
            "id": 0,
            "name": "Hodges Mendoza"
          },
          {
            "id": 1,
            "name": "Frank Vincent"
          },
          {
            "id": 2,
            "name": "Susanne Shields"
          }
        ],
        "greeting": "Hello, Meredith! You have 10 unread messages.",
        "favoriteFruit": "apple"
      },
      {
        "_id": "5c1ce5049a603dc7d568d1ee",
        "index": 2,
        "guid": "9b414c51-8481-4a45-b7a0-47032907b882",
        "isActive": true,
        "balance": "$1,046.59",
        "picture": "http://placehold.it/32x32",
        "age": 27,
        "eyeColor": "brown",
        "name": {
          "first": "Louella",
          "last": "Hickman"
        },
        "company": "ZOLAR",
        "email": "louella.hickman@zolar.co.uk",
        "phone": "+1 (818) 448-3521",
        "address": "887 Madoc Avenue, Caledonia, Illinois, 1430",
        "about": "Cupidatat elit magna consequat velit et ipsum excepteur Lorem officia cillum. Do ullamco sunt do ad nulla Lorem. Ex adipisicing exercitation magna dolor labore proident consectetur ipsum laboris aliquip duis sint velit. Exercitation dolore aliquip reprehenderit proident aliquip. Cupidatat velit aute duis dolor excepteur est enim do officia anim dolore irure.",
        "registered": "Tuesday, June 14, 2016 1:27 PM",
        "latitude": "-33.733293",
        "longitude": "16.10054",
        "tags": [
          "esse",
          "non",
          "sit",
          "qui",
          "exercitation"
        ],
        "range": [
          0,
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9
        ],
        "friends": [
          {
            "id": 0,
            "name": "Justice Johns"
          },
          {
            "id": 1,
            "name": "Cindy Houston"
          },
          {
            "id": 2,
            "name": "Corinne Morse"
          }
        ],
        "greeting": "Hello, Louella! You have 8 unread messages.",
        "favoriteFruit": "strawberry"
      },
      {
        "_id": "5c1ce504ce78e321dcfd11f4",
        "index": 3,
        "guid": "212d2a7a-a32f-4811-bc58-42f9d501e892",
        "isActive": false,
        "balance": "$3,069.51",
        "picture": "http://placehold.it/32x32",
        "age": 28,
        "eyeColor": "green",
        "name": {
          "first": "Ora",
          "last": "Frye"
        },
        "company": "TERRAGEN",
        "email": "ora.frye@terragen.me",
        "phone": "+1 (897) 597-2792",
        "address": "661 Applegate Court, Jacksonburg, Georgia, 1621",
        "about": "Est dolor dolor labore minim. Ut reprehenderit cupidatat eu duis in ea. Esse elit et consequat do sunt do nisi laborum duis qui labore. Labore cupidatat laboris ullamco enim proident quis occaecat in duis enim. Cillum dolore tempor non id laboris duis. Eiusmod quis culpa exercitation culpa anim id ex officia est minim. Nisi dolore reprehenderit eu deserunt eiusmod velit exercitation id ipsum cupidatat.",
        "registered": "Friday, August 19, 2016 3:02 AM",
        "latitude": "64.182589",
        "longitude": "76.446986",
        "tags": [
          "veniam",
          "id",
          "enim",
          "id",
          "eiusmod"
        ],
        "range": [
          0,
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9
        ],
        "friends": [
          {
            "id": 0,
            "name": "Corina Hernandez"
          },
          {
            "id": 1,
            "name": "Hale Aguilar"
          },
          {
            "id": 2,
            "name": "Millie Walter"
          }
        ],
        "greeting": "Hello, Ora! You have 7 unread messages.",
        "favoriteFruit": "banana"
      },
      {
        "_id": "5c1ce5043e3576394cb7c013",
        "index": 4,
        "guid": "8cad3eef-f636-47ec-96fa-6dd84cbeddff",
        "isActive": false,
        "balance": "$1,405.74",
        "picture": "http://placehold.it/32x32",
        "age": 39,
        "eyeColor": "green",
        "name": {
          "first": "Shelly",
          "last": "Stevens"
        },
        "company": "OMNIGOG",
        "email": "shelly.stevens@omnigog.name",
        "phone": "+1 (886) 592-2410",
        "address": "757 Perry Terrace, Duryea, Arizona, 6422",
        "about": "Ullamco pariatur nulla reprehenderit ex dolor eu. Tempor aliqua commodo consectetur non qui dolor incididunt excepteur duis laboris amet. Esse non adipisicing est exercitation qui quis exercitation ad esse labore ex pariatur. Pariatur ut aliquip magna irure sunt velit labore. Consectetur laborum in non et reprehenderit.",
        "registered": "Friday, August 25, 2017 4:55 PM",
        "latitude": "-70.359401",
        "longitude": "67.882877",
        "tags": [
          "pariatur",
          "aliqua",
          "aute",
          "duis",
          "officia"
        ],
        "range": [
          0,
          1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9
        ],
        "friends": [
          {
            "id": 0,
            "name": "Goodwin Duncan"
          },
          {
            "id": 1,
            "name": "Mable Hale"
          },
          {
            "id": 2,
            "name": "Cathryn English"
          }
        ],
        "greeting": "Hello, Shelly! You have 9 unread messages.",
        "favoriteFruit": "banana"
      }
    ];
  }
}

module.exports = TestHTTPGet;
