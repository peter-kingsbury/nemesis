'use strict';

const Process = require('../core/Process');

class SlowStartingProcess extends Process {
  constructor(api) {
    super(api);

    this.name ='slow starting process';
    this.allowed = ['web'];
  }

  async init() {
    await super.init();

    const timeout = 0;

    return new Promise(resolve => {
      this.api.log(`waiting ${timeout} ms ...`);
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }
}

module.exports = SlowStartingProcess;
