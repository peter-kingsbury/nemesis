'use strict';

const Process = require('../core/Process');

class PeriodicTask extends Process {
  constructor(api) {
    super(api);

    this.name ='task every 6 seconds';
    this.allowed = ['task'];
    this.priority = 1100;
    this.schedule = '*/6 * * * * *';
  }

  async exec(data) {
    this.api.log(`Running '${this.name}' with ${JSON.stringify(data)}'`);
    return {};
  }
}

module.exports = PeriodicTask;
