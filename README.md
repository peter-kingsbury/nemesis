# Nucleus Toolkit (NTK) ⚛

## Usage

Run `npm start` to begin NTK.

## Development and Debugging

Always start NTK in debug mode, using the following:

1. Launch the Node process in debug mode:
```javascript
$ npm run debug
```

2. Open the Chrome Developer Tools

Point your browser to chrome://inspect

3. Open dedicated DevTools for Node

Click on `Open dedicated DevTools for Node` ... do _not_ use the `Inspect` link; instead .

A new window will open. From here, you may perform the following:
* code inspection
* breakpointing
* stack, context and variable inspection
* cpu and memory profiling

**To perform code inspection, drill down into Node.js > file:// > /path/to/code > module name**

You may close and reopen the debug session as often as is necessary, without the need to restart NTK.

4. Start NTK from the debugger

On first start, NTK will be halted on the first line of code. You may optionally place a breakpoint anywhere in the code that you want the interpreter to halt on, then click the Play button to resume normal operation.

## Design Rationale

See [The Twelve-Factor App](https://12factor.net/)

### I. Codebase

> One codebase tracked in revision control, many deploys

NTK is tracked in Bitbucket.

All derivative applications shall lock their functionality to a specific major.minor.patch version, depending on need.

Developers of NTK shall make every attempt to preserve the integrity of derivative applications on a per-feature basis.

### II. Dependencies

> Explicitly declare and isolate dependencies

**All dependencies will be loaded statically.**

Violating this prevents the build system from rooting code in a secure sandbox.

Pull requests which introduce dynamically-imported dependencies will be challenged.

Dependencies are version-locked to "major-minor-patch" level.

All pull requests which introduce new dependencies _must_ lock [semantic versioning](https://docs.npmjs.com/about-semantic-versioning) to "major-minor-patch" level.

### III. Config

> Store config in the environment

**All configs stored in the environment shall be supplied by the environment.**

Config file management is done as follows:

1. Load a locally-stored config which contains all possible options which has acceptable defaults applied.
2. Load an application-specific config which overrides the default config.
3. Finally, sift through `process.env` entries looking for underscored-notation overrides for application config.

For example, with the following config compliment:

```javascript
// default.json
{
    "servers": {
        "web": {
            "enabled": true,
            "listen": 8080,
            "timeout": 30000
        }
    }
}
```

```javascript
// application.json
{
    "servers": {
        "web": {
            "timeout": 10000
        }
    }
}
```

```bash
$ APP_SERVERS_WEB_LISTEN=8081 node ./bin/nucleus.js
```

NTK will merge environment -> application -> default, rendering the web server enabled, its timeout for connections set to 10 seconds, and its listening port set to 8081.

This allows us to (1) establish a config schema usable by the core application, (2) provide application-instance specific overrides, and (3) allow further deployment-specific overrides to be applied.

### IV. Backing services

> Treat backing services as attached resources

Code accessing external services should not need to be changed between deployments on local vs deployments in cloud.

NTK shall access external resources through its default+specific+environment config system.

### V. Build, release, run

> Strictly separate build and run stages

`npm run build` will produce executable binaries suitable for production deploys with all applicable production-like and optimization flags enabled.

`npm run release` will run NTK in "release mode" with all applicable production-like and optimization flags enabled.

`npm start` will run the executable binary with no additional configurations or constraints.

Derivative applications must implement their own build process, as the default NTK build process is insufficient as a deployable application.

### VI. Processes

> Execute the app as one or more stateless processes

All states must be pulled from an external, more permanent storage (cache, database, etc).

NTK shall not maintain any state.

The _only_ acceptable exception to this rule is that of health-state. NTK allows derivative applications to register health-checks for dependent services, and apply functionality constraints if deemed unhealthy.

### VII. Port binding, Data isolation

> Export services via port binding

The main type of service exposed by NTK is that of an HTTP service, defined via configuration file.

NTK's design allows the developer to turn-on and -off different services via configuration file.

Regarding data isolation, NTK will only ever access _its own database_ and never that of another database, except through an exposed API offered by that other application.

### VIII. Concurrency

> Scale out via the process model

When run as a consumer of scheduled tasks, NTK _must_ ensure uniqueness for tasks which _might_ overlap. This is done via Agenda's `unique` option used when scheduling jobs (or called and saved post-creation).

### IX. Disposability

> Maximize robustness with fast startup and graceful shutdown

With minimal configuration, NTK takes <100ms to initialize and come to a state where it is ready to operate.

When receiving a kill signal, NTK makes every attempt to shut down gracefully.

Minimal application state should be stored; instead, any sort of state should be derived from persistant storage.

As per best Node.js practises, uncaught promise rejections are handled via process termination.

### X. Dev/prod parity

> Keep development, staging, and production as similar as possible

All development must be verified _at a minimum_ with a Dockerized instance of NTK before publishing code for peer review.

### XI. Logs

> Treat logs as event streams

NTK uses Winston for logging, and only outputs log content to console. No log files are written unless this feature is explicitly turned _on_ via config.

Absolutely _no_ usage of console.log will be permitted through peer review. Instead, utilize the built-in logging facility.

### XII. Admin processes

> Run admin/management tasks as one-off processes

Tasks related to administrative processes (integration testing, data migrations) will be performed _separately_ from NTK. No functionality will exist inside NTK which facilitates these purposes.
