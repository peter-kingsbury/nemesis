'use strict';

const util = require('util');
const fs = require('fs');
const path = require('path');
const dot = require('dot-object');
const deepmerge = require('deepmerge');

const fsAccessAsync = util.promisify(fs.access);

const loadFile = async (file) => {
  try {
    await fsAccessAsync(file, fs.constants.R_OK);
  } catch (e) {
    return null;
  }

  // eslint-disable-next-line security/detect-non-literal-require
  return require(file);
};

class Config {
  // noinspection JSMethodCanBeStatic
  async overrideKeysFromEnvironment(cfg) {
    let vars = dot.dot(cfg);
    let varsMap = new Map(Object.entries(vars));

    // First pass, check for like-named environment variables
    let prefix = cfg['app']['environmentPrefix'];
    let envMap = new Map(Object.entries(process.env));
    for (const key of Object.keys(vars)) {
      let envKey = key.replace(/\./g, '_').toUpperCase();
      envKey = `${prefix}_${envKey}`;

      let envValue = envMap.get(envKey);

      if (typeof envValue === 'undefined') {
        continue;
      }

      if (typeof envValue === 'string') {
        if (envValue === 'true' || envValue === 'false') {
          // coerce booleans
          varsMap.set(key, envValue === 'true');
        } else if (Number.isSafeInteger(envValue)) {
          // convert integers
          varsMap.set(key, parseInt(envValue));
        } else if (!isNaN(parseFloat(envValue))) {
          // convert floating point numbers
          varsMap.set(key, parseFloat(envValue));
        } else {
          // use direct strings
          varsMap.set(key, envValue);
        }
      }
    }

    let obj = Object.create(null);
    for (let [k, v] of varsMap) {
      // eslint-disable-next-line security/detect-object-injection
      obj[k] = v;
    }

    return dot.object(obj);
  }

  async load() {
    let defaultCfg = await loadFile(path.join(__dirname, 'config.default.json'));
    let cwdConfig = await loadFile(path.join(process.cwd(), 'config', 'config.json'));
    return await this.overrideKeysFromEnvironment(deepmerge(defaultCfg, cwdConfig || {}));
  }
}

module.exports = Config;
