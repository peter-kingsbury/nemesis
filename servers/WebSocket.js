'use strict';

const Server = require('../core/Server');
const Primus = require('primus');
const uuid = require('uuid');

class WebSocket extends Server {
  constructor(api, httpServer) {
    super(api);

    this._http = httpServer;
    this._primus = new Primus(this._http, {
      pathname: 'primus',
      parser: 'JSON',
      idGenerator: this.generateId.bind(this),
      compression: true,
      maxLength: 1024
    });

    this._primus.on('connection', this.onClientConnected.bind(this));
    this._primus.on('disconnection', this.onClientDisconnected.bind(this));
  }

  async start() {
  }

  async stop() {
  }

  onClientConnected(connection) {
    /*
    * */
    this.api.log('WS client connected');
  }

  onClientDisconnected(connection) {
    this.api.log('WS client disconnected');
  }

  generateId() {
    return uuid.v4();
  }
}

module.exports = WebSocket;

// TODO: implementation


/*
curl --include --no-buffer --header "Connection: Upgrade" --header "Upgrade: websocket" --header "Host: localhost:8080" --header "Origin: http://localhost:80" --header "Sec-WebSocket-Key: SGVsbG8sIHdvcmxkIQ==" --header "Sec-WebSocket-Version: 13" http://localhost:8080/primus
*/
