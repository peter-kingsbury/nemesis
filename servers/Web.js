'use strict';

const http = require('http');
const Server = require('../core/Server');
const express = require('express');
const compression = require('compression');

class Web extends Server {
  constructor(api) {
    super(api);

    this._app = express();
    this._server = http.createServer(this._app);
    this._port = this.api.config.servers.web.listen;

    const c11n = this.api.config.servers.web.compression;
    if (c11n.enabled === true) {
      // TODO: Defaults require tweaking
      this._app.use(compression({
        chunkSize:  c11n.chunkSize,
        level:      c11n.level,
        memLevel:   c11n.memLevel,
        threshold:  c11n.threshold,
        windowBits: c11n.windowBits
      }));
      this.api.log(`Web server compression enabled (chunkSize=${c11n.chunkSize} level=${c11n.level} memLevel=${c11n.memLevel} threshold=${c11n.threshold} windowBits=${c11n.windowBits})`);
    }
  }

  get port() {
    return this._port;
  }

  get server() {
    return this._server;
  }

  async start() {
    // TODO: Spawn one child process per core
    await super.start();
    this._app.get('/status', (req, res) => res.send('OK'));
    this._app.get('/ready', (req, res) => res.send('OK'));
    this._app.listen(this.port, this.onStart.bind(this));
  }

  async onStart() {
    this.api.log(`Webserver listening on :${this.port}`);
  }

  async stop() {
    await super.stop();
    // TODO: This errors out with "Error: Not running" on server.close -- why?
    /*return new Promise((resolve, reject) => {
      this._server.close(err => {
        if (err) {
          return reject(err);
        }
        resolve();
      });
    });*/
  }

  addRoute(method, route, handler) {
    this.api.log(`Adding handler for '${method} ${route}'`);
    // eslint-disable-next-line security/detect-object-injection
    this._app[method.toLowerCase()](route, handler);
  }
}

module.exports = Web;
