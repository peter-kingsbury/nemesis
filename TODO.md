# TODO

A list of items, priority-sequenced, that are necessary to realize Nucleus.

## Priority

- [x] Figure out packaging. Currently using `pkg`, but it has some path issues.
  - Got it working. Needed to add source files to pkg manifest in package.json
- [ ] Set up a Nucleus example application which instantiates one each of HTTP, WebSocket and GRPC endpoints.
- [ ] Configure index.js signal monitoring for SIGHUP, SIGKILL
