# Definitions

The following is a list of terminology and definitions, provided to establish a common language when referring to various aspects of the application.

**A Note About Formatting**

Entries in the definitions list should use the following format:

* First line consists of level-six-title terminology, followed by a newline.
* Second line consists of descriptive, brief language which defines what the aspect is, does or how it behaves.

## List of Definitions

###### Server
A part of the application which binds to a listening port, and accepts incoming connections. Incoming requests are handed-off to the RPI.

###### Request-Process Interface (RPI)
A translation-medium which intakes incoming request from an externally-exposed interface, and converts it to a format consumable by a Process.

###### Process
An operation that can be completed by the application. Results bubble upward back to the RPI.

###### Component
A part of the application which typically is long-running.

###### Job

A running instance of a Process which has been previously scheduled by the TaskManager.


